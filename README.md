# Multi-source Multi-domain Sentiment Analysis with BERT-based Models

This is the official repository for the paper **Multi-source Multi-domain Sentiment Analysis with BERT-based
Models** ([link to the paper](http://www.lrec-conf.org/proceedings/lrec2022/pdf/2022.lrec-1.62.pdf)) presented at *LREC
2022*.

## Installation

The file environment.yml contains a Conda env
to [automatically](https://conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html#creating-an-environment-from-an-environment-yml-file)
install all the relevant packages needed to run our code.

## Our model

To use our model `AMC-opt-msmd`, you can use the "PyTorch" method or via HuggingFace Hub.



### "PyTorch" method

You have to download the weights from here and put them in the `data/models`:
- [AMC-opt-msmd](https://drive.google.com/file/d/11Gvnm7tQ_oQD9erRIHirieT8R96KChP4/view?usp=sharing)

Then, you can use the class `SentimentAnalyzer` in the module `sentiment_analyzer_api.py` to easily retrieve the sentiment
of your sentences.

An example of usage is the following:

```python
from sentiment_analyzer_api import SentimentAnalyzer

""" SentimentAnalyzer
        :param str weights_path: path to pretrained weights,
            default='data/models/amc_opt_msmd.pt'
        :param str tok_path:     path/url to pretrained tokenizer
            default="m-polignano-uniba/bert_uncased_L-12_H-768_A-12_italian_alb3rt0"
        :param str max_lenght:   max_lenght of the tokenizer
            default=128
        :param str device:       either cpu or a cuda device
            default='cuda:0'
"""
sa = SentimentAnalyzer()
sa(["L'ultimo film di Star Wars mi è piaciuto da morire",
    "Ieri ho litigato con il mio capo",
    "La borsa di Tokyo perde il 2%",
    "Oggi sono andato al supermercato"])

outuput: ["positive", "negative", "negative", "neutral"]
```

### Hugging Face hub
If you want to use our model from the HuggingFace hub, this is the code that you have to run:

```python
from transformers import AutoTokenizer, AutoModelForSequenceClassification

tokenizer = AutoTokenizer.from_pretrained("m-polignano-uniba/bert_uncased_L-12_H-768_A-12_italian_alb3rt0")
tokenizer.model_max_length = 128

model = AutoModelForSequenceClassification.from_pretrained('SISLab/amc-opt-msmd', trust_remote_code=True)

sentences = ["L'ultimo film di Star Wars mi è piaciuto da morire",
             "Ieri ho litigato con il mio capo",
             "La borsa di Tokyo perde il 2%",
             "Oggi sono andato al supermercato"]

sa = SentimentAnalyzerHF(model, tokenizer, device='cuda:0')
sa(sentences)

outuput: ["positive", "negative", "negative", "neutral"]
```

## Reproducibility

### Models

The off-the-shelf models that have been used in the paper are the following:

- [AlBERTo-IT](https://github.com/marcopoli/AlBERTo-it)
- [FEEL-IT](https://github.com/MilaNLProc/feel-it)

Then, we fine-tuned and optimized *AlBERTo-IT* on the Sentiment Analysis task, we call this model **AlBERTo Multi Class
optimized** (AMC-opt). Fine-tuning AMC-opt on all the datasets that we proposed, we achieved better results than
fine-tuning the model in isolation on every single dataset. We called this fine-tuned model **AMC-opt multi-source
multi-domain (AMC-opt-msmd)**. In this repo, we release AMC-opt-msmd but it has been trained on all the dataset except
on COADAPT valence. The reason of this is privacy issues. Nevertheless, the performances are close to those
reported in the paper.

### Scripts
The code needed to reproduce our experiments is written in the Jupyter Notebook called `Multi-domain Multi-source Analysis.ipynb`.

### Data

To retrain the model or to check the performance, you have to download the following datasets in `data/`:

- [SENTIPOLC16](http://www.di.unito.it/~tutreeb/sentipolc-evalita16/index.html): `data/Sentipolc16`
- [FEEL-IT](https://github.com/MilaNLProc/feel-it): `data/Feel-it`
- [MultiEmotions-IT](https://github.com/RacheleSprugnoli/Esercitazioni_SA/tree/master/dataset): `data/Multiemotions-it`
- Amazon reviews: Private datasets. `data/Amazon-reviews`
- Coadapt Valence: Private datasets. `data/Coadapt_valence`
- [AriEmozione 1.0](https://zenodo.org/record/4022318#.YRTaYYgzZPY): `data/Aria`
- [TRIP-MAML](https://github.com/diegma/trip-maml): `data/Trip-maml`
- Our dataset [ITFN](https://sisl.disi.unitn.it/itfn-corpus/): `data/ITFN`

**Attention!** the model that we released  `AMC-opt-msmd` is different from that we used in the paper since it is not
trained on COADAPT valence dataset due to privacy issues.

The results that we achieved removing COADAPT valence from the training set are the following which are compare to those presented in the paper:\


| Model                                 | SENTIPOLC16 | ITFN       | FEEL-IT | MultiE.    | Amazon     | Trip-M.    | Ari        | COADAPT    |
|---------------------------------------|-------------|------------|---------|------------|------------|------------|------------|------------|
| with COADAPT v. (paper results)       | 0.62/0.82*  | 0.64/0.84* | 0.87    | 0.74/0.89* | 0.63/0.88* | 0.67/0.94* | 0.75/0.76* | 0.77/0.92* |
| without COADAPT v. (model we release) | 0.62/0.78*  | 0.63/0.83* | 0.85    | 0.72/0.89* | 0.64/0.89* | 0.72/0.93* | 0.76/0.78* | 0.77/0.91* |

## Cite us 😉

If you are using our model in your research or work, we kindly ask you to cite us:

```cite
@InProceedings{roccabruna-azzolin-riccardi:2022:LREC,
  author    = {Roccabruna, Gabriel  and  Azzolin, Steve  and  Riccardi, Giuseppe},
  title     = {Multi-source Multi-domain Sentiment Analysis with BERT-based Models},
  booktitle      = {Proceedings of the Language Resources and Evaluation Conference},
  month          = {June},
  year           = {2022},
  address        = {Marseille, France},
  publisher      = {European Language Resources Association},
  pages     = {581--589},
  abstract  = {Sentiment analysis is one of the most widely studied tasks in natural language processing. While BERT-based models have achieved state-of-the-art results in this task, little attention has been given to its performance variability across class labels, multi-source and multi-domain corpora. In this paper, we present an improved state-of-the-art and comparatively evaluate BERT-based models for sentiment analysis on Italian corpora. The proposed model is evaluated over eight sentiment analysis corpora from different domains (social media, finance, e-commerce, health, travel) and sources (Twitter, YouTube, Facebook, Amazon, Tripadvisor, Opera and Personal Healthcare Agent) on the prediction of positive, negative and neutral classes. Our findings suggest that BERT-based models are confident in predicting positive and negative examples but not as much with neutral examples. We release the sentiment analysis model as well as a newly financial domain sentiment corpus.},
  url       = {https://aclanthology.org/2022.lrec-1.62}
}
```

Thank you! 😁
